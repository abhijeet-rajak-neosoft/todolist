
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Dashboard from './component/Dashboard';
import Login from './component/Login';
import Register from './component/Register';
import NavBar from "./component/Navbar";

function App() {
  return (
    <Router>
      <NavBar />
      <Switch>

        <Route exact path="/login" component={Login} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/register" component={Register} />
        <Redirect to="/login" />
      </Switch>


    </Router>

  );
}

export default App;
