import React, { Component } from 'react'

const withProtected = (WrappedComponent) => {

    return class extends Component {
        componentDidMount() {
            if (!localStorage.getItem("_user"))
                this.props.history.push("/login");
            else
                this.props.history.push("/dashboard");
        }
        render() {
            return (
                <WrappedComponent {...this.props} />
            )
        }
    }

}

export default withProtected
