import React, { Component } from 'react';
import { Box, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, CardContent, CardHeader, Card, Button, Typography, FormHelperText, Alert } from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import withProtected from '../hoc/withProtected';
import { Axios } from '../utils/axios';
// import { users } from '../../user.json';

export const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export class Login extends Component {

    state = {
        showPassword: false,
        password: "",
        email: "",
        userExists: true,
        error: {
            email: "",
            password: ""
        }
    }



    handleChange = (e) => {
        const { name, value } = e.target;
        const { error } = this.state;

        this.setState({
            [name]: value
        });

        switch (name) {
            case 'email':
                error[name] = emailRegex.test(value) ? "" : "Invalid Email"
                break;
            case "password":
                error[name] = value.length > 8 ? "" : "Invalid Password"
                break;
        }

        this.setState({
            error
        });

    }
    handleClickShowPassword = (e) => {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }
    handleSubmit = async (e) => {
        console.log("submit is called");
        e.preventDefault();
        const { email, password } = this.state;


        const userExists = await Axios.get(`/users?email=${email}&password=${password}`);

        // console.log("USER EXISTS", typeof userExists.data.length);

        if (!userExists.data.length > 0) {
            this.setState({ userExists: false });
            setTimeout(() => { this.setState({ userExists: true }) }, 3000);
            return

        }

        // this.props.history.push("/dashboard");

        localStorage.setItem("_user", JSON.stringify({id: userExists.data[0].id ,email: userExists.data[0].email}));

        this.props.history.push("/dashboard");
    }
    render() {
        const { showPassword, password, email } = this.state;
        return (
            <>

                <Card sx={{ mx: "auto", mt: 5, width: 500, }}>
                    {!this.state.userExists && <Alert severity="error">Inavlid Email & Password</Alert>}
                    <CardHeader align="center" sx={{}} title="Login Form">
                    </CardHeader>
                    <CardContent sx={{ maxWidth: 600 }}>
                        <Box sx={{ minWidth: 275, borderColor: 'secondary.main' }}>
                            <form onSubmit={this.handleSubmit}>

                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    <InputLabel htmlFor="outlined-adornment-email">Email</InputLabel>
                                    <OutlinedInput
                                        error={this.state.error.email.length > 0 ? true : false}
                                        name="email"
                                        id="outlined-adornment-email"
                                        value={email}
                                        onChange={this.handleChange}
                                        label="Email"
                                    />
                                    {this.state.error.email.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.email}</FormHelperText>}

                                </FormControl>

                                <FormControl sx={{ mt: 1, width: '100%' }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        error={this.state.error.password.length > 0 ? true : false}
                                        name="password"
                                        id="outlined-adornment-password"
                                        type={showPassword ? 'text' : 'password'}
                                        value={password}
                                        onChange={this.handleChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={this.handleClickShowPassword}
                                                    aria-label="toggle password visibility"
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                    {this.state.error.password.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.password}</FormHelperText>}

                                </FormControl>

                                <Typography align='center'>
                                    <Button type="submit" sx={{ mt: 3, width: "50%" }} style={{ justifyContent: "center" }} variant="contained" color="success">
                                        Login
                                    </Button>
                                </Typography>


                            </form>
                        </Box>
                    </CardContent>
                </Card>
            </>
        )
    }
}

export default withProtected(Login)
