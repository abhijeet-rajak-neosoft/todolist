import React, { Component } from 'react';
import { Box, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, CardContent, CardHeader, Card, Button, Typography, FormHelperText, Alert } from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { emailRegex } from './Login';
import { Axios } from '../utils/axios';

export class Register extends Component {

    state = {
        firstName: "",
        lastName: "",
        email: "",
        userName: "",
        password: "",
        confirmPassword: "",
        showPassword: false,
        showConfirmPassword: false,
        error: {
            firstName: "",
            lastName: "",
            email: "",
            userName: "",
            password: "",
            confirmPassword: ""
        }
    }
    handleChange = (e) => {
        const { name, value } = e.target;
        const { error } = this.state;

        this.setState({
            [name]: value
        });


        // console.log("firstname", this.state.firstName.length);

        // console.log("lastname", this.state.lastName.length);
        switch (name) {

            case 'firstName':
                error[name] = value.length > 0 ? "" : "First Name is required"
                break;
            case 'lastName':
                error[name] = value.length > 0 ? "" : "Last Name is required"
            case 'userName':
                error["userName"] = this.state.firstName.length > 0 && this.state.lastName.length > 0 ? "" : "Invalid user name"
                break
            case 'email':
                error[name] = emailRegex.test(value) ? "" : "Invalid email"
                break;
            case 'password':
                error[name] = value.length > 8 ? "" : "Length of password should be greater than 8"
                break;
            case 'confirmPassword':
                error[name] = value === this.state.password ? "" : "Password & Confirm Password donot match"
                break;
            default:
        }


        this.setState({
            error
        });

    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const { error } = this.state;

        const { firstName, lastName, email, userName, password, confirmPassword } = this.state;

        // first check empty value

        // then check suitable for submit or not

        const excludedFields = ["showPassword", "showConfirmPassword", "error"];

        Object.keys(this.state).forEach(key => !excludedFields.includes(key) && !this.state[key].length > 0 ? error[key] = `${key} is required` : "");

        const noError = Object.keys(error).find(key => error[key].length > 0)

        // console.log("NO ERROR", noError);


        if (!noError) {
            const user = await Axios.post("/users", {
                firstName,
                lastName,
                userName,
                password,
                email,
            });

            // console.log("USER", user);

            // alert("Submit");
            this.setState({
                firstName: "",
                lastName: "",
                userName: "",
                password: "",
                email: "",
                confirmPassword: ""
            });

            this.props.history.push("/login");
            return
        }

        this.setState({
            error
        });

    }

    handleBlur = (e) => {

        const { error, lastName, firstName } = this.state;
        if (!lastName.length > 0 || !firstName.length > 0) {
            error["userName"] = "Invalid user name";
            error["lastName"] = lastName.length === 0 ? "lastName is required" : "";
            error["firstName"] = firstName.length === 0 ? "firstName is required" : "";
            this.setState({
                userName: this.state.firstName + " " + this.state.lastName,
                error
            })
            return
        }
        this.setState({
            userName: this.state.firstName + " " + this.state.lastName,
            error: {
                ...error,
                userName: ""
            }
        })
    }

    handleClickShowPassword = (value) => {

        this.setState({ [value]: !this.state[value] })
    }
    render() {
        // console.log("RENDERIGN", this.state);
        const { firstName, lastName, email, userName, password, confirmPassword, showPassword, showConfirmPassword } = this.state;
        return (
            <>

                <Card sx={{ mx: "auto", mt: 5, width: 500, }}>
                    <CardHeader align="center" sx={{}} title="Register   Form">
                    </CardHeader>
                    <CardContent sx={{ maxWidth: 600 }}>
                        <Box sx={{ minWidth: 275, borderColor: 'secondary.main' }}>
                            <form onSubmit={this.handleSubmit}>

                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    <InputLabel htmlFor="outlined-adornment-email">First Name</InputLabel>
                                    <OutlinedInput
                                        error={this.state.error.firstName.length > 0 ? true : false}
                                        name="firstName"
                                        id="outlined-adornment-email"
                                        value={firstName}
                                        onChange={this.handleChange}
                                        label="First Name"
                                    />
                                    {this.state.error.firstName.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.firstName}</FormHelperText>}

                                </FormControl>


                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    <InputLabel htmlFor="outlined-adornment-email">Last Name</InputLabel>
                                    <OutlinedInput
                                        onBlur={this.handleBlur}
                                        error={this.state.error.lastName.length > 0 ? true : false}
                                        name="lastName"
                                        id="outlined-adornment-email"
                                        value={lastName}
                                        onChange={this.handleChange}
                                        label="Last Name"
                                    />
                                    {this.state.error.lastName.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.lastName}</FormHelperText>}

                                </FormControl>

                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    <InputLabel htmlFor="outlined-adornment-email">User Name</InputLabel>
                                    <OutlinedInput

                                        error={this.state.error.userName.length > 0 ? true : false}
                                        name="userName"
                                        id="outlined-adornment-email"
                                        value={userName}
                                        onChange={this.handleChange}
                                        label="User Name"
                                        disabled
                                    />
                                    {this.state.error.userName.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.userName}</FormHelperText>}

                                </FormControl>


                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    <InputLabel htmlFor="outlined-adornment-email">Email</InputLabel>
                                    <OutlinedInput
                                        error={this.state.error.email.length > 0 ? true : false}
                                        name="email"
                                        id="outlined-adornment-email"
                                        value={email}
                                        onChange={this.handleChange}
                                        label="Email"
                                    />
                                    {this.state.error.email.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.email}</FormHelperText>}

                                </FormControl>

                                <FormControl sx={{ mt: 1, width: '100%' }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        error={this.state.error.password.length > 0 ? true : false}
                                        name="password"
                                        id="outlined-adornment-password"
                                        type={showPassword ? 'text' : 'password'}
                                        value={password}
                                        onChange={this.handleChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() => this.handleClickShowPassword("showPassword")}
                                                    aria-label="toggle password visibility"
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                    {this.state.error.password.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.password}</FormHelperText>}

                                </FormControl>


                                <FormControl sx={{ mt: 1, width: '100%' }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Confirm Password</InputLabel>
                                    <OutlinedInput
                                        error={this.state.error.confirmPassword.length > 0 ? true : false}
                                        name="confirmPassword"
                                        id="outlined-adornment-password"
                                        type={showConfirmPassword ? 'text' : 'password'}
                                        value={confirmPassword}
                                        onChange={this.handleChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() => this.handleClickShowPassword("showConfirmPassword")}
                                                    aria-label="toggle password visibility"
                                                    edge="end"
                                                >
                                                    {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Confirm Password"
                                    />
                                    {this.state.error.confirmPassword.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.confirmPassword}</FormHelperText>}

                                </FormControl>

                                <Typography align='center'>
                                    <Button type="submit" sx={{ mt: 3, width: "50%" }} style={{ justifyContent: "center" }} variant="contained" color="success">
                                        Register
                                    </Button>
                                </Typography>


                            </form>
                        </Box>
                    </CardContent>
                </Card>
            </>
        )
    }
}

export default Register
