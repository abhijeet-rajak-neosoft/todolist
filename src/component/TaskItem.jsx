import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import ClearIcon from '@mui/icons-material/Clear';
import CheckIcon from '@mui/icons-material/Check';


function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box mt={3} sx={{ flexShrink: 0,}}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

function createData(task, priority) {
    return { task, priority };
}

// const rows = [
//     createData('Cupcake', 3),
//     createData('Donut', 25),
//     createData('Eclair', 16),
//     createData('Frozen yoghurt', 6),
//     createData('Gingerbread', 16.0),
//     createData('Honeycomb', 3),
//     createData('Ice cream sandwich', 9),
//     createData('Jelly Bean', 0),
//     createData('KitKat', 26),
//     createData('Lollipop', 2),
//     createData('Marshmallow', 0),
//     createData('Nougat', 19.0),
//     createData('Oreo', 18),
// ].sort((a, b) => (a.priority < b.priority ? -1 : 1));


export class TaskItem extends Component {

    state = {
        page: 0,
        rowsPerPage: 5
    }
    // const[page, setPage] = React.useState(0);
    // const[rowsPerPage, setRowsPerPage] = React.useState(5);

    // Avoid a layout jump when reaching the last page with empty rows.
    emptyRows =
        this.state.page > 0 ? Math.max(0, (1 + this.state.page) * this.state.rowsPerPage - this.props.tasks.length) : 0;

    handleChangePage = (event, newPage) => {
        // setPage(newPage);
        this.setState({
            page: newPage
        })
    };

    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: parseInt(event.target.value, 10)
        })
        // setRowsPerPage(parseInt(event.target.value, 10));
        // setPage(0);
        this.setState({
            page: 0
        })
    };
    render() {
        const rows = this.props.tasks.sort((a, b) => (a.priority < b.priority ? 1 : -1));
        const { rowsPerPage, page } = this.state;
        return rows.length > 0 && (
            <Box>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
                        <TableBody>
                            {(rowsPerPage > 0
                                ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                : rows
                            ).map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell style={{textDecoration:row.completed ?"line-through":""}} component="th" scope="row">
                                        {row.task}
                                    </TableCell>
                                    <TableCell style={{ width: 160 }}>
                                        <ClearIcon onClick={(e)=>this.props.onDeleteTask(row.id, e)} style={{color:"red", marginRight:"20px", cursor:"pointer"}} />  
                                        <CheckIcon onClick={(e)=>this.props.onCompleteTask(row.id,e)} style={{color:"green", cursor:"pointer"}} />

                                    </TableCell>
                                </TableRow>
                            ))}

                            {this.emptyRows > 0 && (
                                <TableRow style={{ height: 53 * this.emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                    colSpan={3}
                                    count={rows.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    SelectProps={{
                                        inputProps: {
                                            'aria-label': 'rows per page',
                                        },
                                        native: true,
                                    }}
                                    onPageChange={this.handleChangePage}
                                    onRowsPerPageChange={this.handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActions}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </Box>
        );
    }
}

export default TaskItem
