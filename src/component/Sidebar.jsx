import React, { Component } from 'react';
import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DraftsIcon from '@mui/icons-material/Drafts';
import SendIcon from '@mui/icons-material/Send';
import StarBorder from '@mui/icons-material/StarBorder';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import { FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, Collapse, Button, Typography, FormHelperText } from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Axios } from '../utils/axios';
// import { users } from '../../user.json';



export class Sidebar extends Component {
    state = {
        listButton: true,
        showPassword: false,
        showConfirmPassword: false,
        password: "",
        confirmPassword: "",
        open: false,
        borderColor: "white",
        color: "white",
        error: {
            password: "",
            confirmPassword: ""
        }
    }
    handleClick = (e) => {
        this.setState({
            open: !this.state.open
        })
    }
    handleChange = (e) => {
        const { value, name } = e.target;
        const { error } = this.state;
        this.setState({
            [name]: value
        });

        switch (name) {
            case "password":
                error[name] = value.length > 8 ? "" : "Password length must be 8 in length"
                break
            case "confirmPassword":
                error[name] = value === this.state.password ? "" : "Password and Confirm Password Donot Match"
        }

        this.setState({
            error
        })
    }
    handleClickShowPassword = (value) => {
        value === "password" ? this.setState({
            showPassword: !this.state.showPassword
        }) : this.setState({
            showConfirmPassword: !this.state.showConfirmPassword
        })

    }
    handleChangePassword = async (e) => {
        e.preventDefault();

        const email = JSON.parse(localStorage.getItem("_user")).email;
        const userExists = await Axios.get(`/users?email=${email}`);

        console.log(userExists);
        if(userExists.data.length > 0){
            const updatedPassword = await Axios.patch("/users/" + userExists.data[0].id, {
                password: this.state.password
            });

            localStorage.removeItem("_user");
            this.props.history.push("/login");
        }

        // console.log("UPDATE PASSWORD", updatedPassword);

        

        // if (!userExists.data.length > 0)
        // users.find(user => email === user.email ? user.password = this.state.password : "");
    }
    render() {
        const { password, confirmPassword } = this.state;
        return (
            <List
                style={{height:"100%"}}
                sx={{ width: '100%', maxWidth: 360, height: "100vh", bgcolor: '#222222', color: "white" }}
                component="nav"
                aria-labelledby="nested-list-subheader"

            >
                <ListItemButton >
                    <ListItemIcon>
                        <SendIcon style={{ color: "white" }} />
                    </ListItemIcon>
                    <ListItemText primary="Products" />
                </ListItemButton>
                <ListItemButton >
                    <ListItemIcon>
                        <DraftsIcon style={{ color: "white" }} />
                    </ListItemIcon>
                    <ListItemText primary="Category" />
                </ListItemButton>
                <ListItemButton >
                    <ListItemIcon>
                        <DraftsIcon style={{ color: "white" }} />
                    </ListItemIcon>
                    <ListItemText primary="Order" />
                </ListItemButton>
                <ListItemButton>
                    <ListItemIcon>
                        <DraftsIcon style={{ color: "white" }} />
                    </ListItemIcon>
                    <ListItemText primary="Feedback" />
                </ListItemButton>
                <ListItemButton onClick={this.handleClick}>
                    <ListItemIcon>
                        <InboxIcon style={{ color: "white" }} />
                    </ListItemIcon>
                    <ListItemText primary="Change password" />
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItemButton sx={{ pl: 4 }}>

                            <form onSubmit={this.handleChangePassword}>
                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    {/* <InputLabel htmlFor="outlined-adornment-password">New Password</InputLabel> */}
                                    <OutlinedInput
                                        // error={this.state.error.password.length > 0 ? true : false}
                                        onClick={(e) => this.setState({ borderColor: "pink", color: "pink" })}
                                        name="password"
                                        id="outlined-adornment-password"
                                        type={this.state.showPassword ? 'text' : 'password'}
                                        value={password}
                                        style={{
                                            color: `${this.state.color}`,
                                            border: `2px solid ${this.state.borderColor}`
                                        }}
                                        placeholder="New Password"
                                        onChange={this.handleChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() => this.handleClickShowPassword("password")}
                                                    aria-label="toggle password visibility"
                                                    edge="end"
                                                >
                                                    {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                    {this.state.error.password.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.password}</FormHelperText>}
                                </FormControl>

                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    {/* <InputLabel htmlFor="outlined-adornment-password">New Password</InputLabel> */}
                                    <OutlinedInput
                                        // error={this.state.error.password.length > 0 ? true : false}
                                        onClick={(e) => this.setState({ borderColor: "pink", color: "pink" })}
                                        name="confirmPassword"
                                        id="outlined-adornment-password"
                                        type={this.state.showConfirmPassword ? 'text' : 'password'}
                                        value={confirmPassword}
                                        style={{
                                            color: `${this.state.color}`,
                                            border: `2px solid ${this.state.borderColor}`
                                        }}
                                        placeholder="Confirm Password"
                                        onChange={this.handleChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() => this.handleClickShowPassword("confirmPassword")}
                                                    aria-label="toggle password visibility"
                                                    edge="end"
                                                >
                                                    {this.state.showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                    {this.state.error.confirmPassword.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{this.state.error.confirmPassword}</FormHelperText>}
                                </FormControl>
                                <Typography align='center'>
                                    <Button type="submit" sx={{ mt: 3 }} style={{ justifyContent: "center" }} variant="contained" color="success">
                                        Change Password
                                    </Button>
                                </Typography>
                            </form>
                        </ListItemButton>
                    </List>
                </Collapse>


            </List>
        )
    }
}

export default Sidebar
