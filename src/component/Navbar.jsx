import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { withRouter } from 'react-router-dom';


export class Navbar extends Component {


    renderLoggedInListItem = () => {
        return <>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Dashboard
            </Typography>
            <Box display="flex">
                <Button color="inherit">
                    <Link to="/dashboard" style={{ textDecoration: "none", color: "white" }}>Home</Link>
                </Button>


                <Typography variant="div" component="div" sx={{ flexGrow: 1, mx: 4, mt:1 }}>
                    Welcome {JSON.parse(localStorage.getItem("_user")).email}
                </Typography>

                <Button onClick={() => {
                    localStorage.removeItem("_user");
                    this.props.history.push("/login");
                }} color="inherit">
                    Logout
                </Button>
            </Box></>
    }

    renderNotLoggedInListItem = () => {
        return <>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Todo
            </Typography>
            <Box display="flex">
                <Button color="inherit">
                    <Link to="/login" style={{ textDecoration: "none", color: "white" }}>Login</Link>
                </Button>
                <Button color="inherit">
                    <Link to="/register" style={{ textDecoration: "none", color: "white" }}>Register</Link>
                </Button>
            </Box></>
    }

    render() {

        // const email = localStorage.getItem("_email");
        return (
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            <MenuIcon />
                        </IconButton>


                        {localStorage.getItem("_user") ? this.renderLoggedInListItem() : this.renderNotLoggedInListItem()}

                    </Toolbar>
                </AppBar>
            </Box>
        );
    }
}

export default withRouter(Navbar)