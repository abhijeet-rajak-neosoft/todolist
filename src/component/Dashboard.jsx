import React, { Component } from 'react';
import Navbar from './Navbar';
import Sidebar from './Sidebar';
import AddTask from './AddTask';
import TaskItem from './TaskItem';
import withProtected from '../hoc/withProtected';
import {Axios} from '../utils/axios';
import { Box, Grid, } from "@mui/material";

export class Dashboard extends Component {

    state={
        tasks:[],
        task:"",
        priority: "",
        error:{
            task:"",
            priority:""
        }
    }

    handleLogout = (e) => {
        localStorage.removeItem("_user");
        this.props.history.push("/login");
    };
    
    componentDidMount(){

        console.log("mounting", JSON.parse(localStorage.getItem("_todo")));
        const userId = JSON.parse(localStorage.getItem("_user")).id;
        let todo = JSON.parse(localStorage.getItem("_todo")) || [];

        if(todo && todo.length > 0)
            todo = todo.filter(t=>t.userId===userId);
        else
            localStorage.setItem("_todo",JSON.stringify(todo));

        this.setState({
            tasks: todo
        });
    }

    handleSubmit=(id,e)=>{
        e.preventDefault();

        const orginalTodo = JSON.parse(localStorage.getItem("_todo"));
        const todo = this.state.tasks.sort((a,b)=> (a.id < b.id ? -1 : 1));
        const { error } = this.state;
        const exludedFields = ["tasks", "error"];

        Object.keys(this.state).forEach(key => !exludedFields.includes(key) && !this.state[key].length > 0 ? error[key] = `${key} is required` : "");

        const Error = Object.keys(error).find(key => error[key].length > 0)

        console.log("Error",Error);

        if(Error){
            this.setState({error});
            return;
        }
            
            

        todo.push({
            id:orginalTodo.length > 0 ? orginalTodo[orginalTodo.length -1 ].id + 1: 1,
            task:this.state.task,
            priority:parseInt(this.state.priority),
            completed:false,
            userId:id
        });
        

        // console.log("orginalTodo", orginalTodo);

        const notCurrentUserTodo = orginalTodo.filter(t=>t.userId!==id);

        localStorage.setItem("_todo", JSON.stringify([...notCurrentUserTodo,...todo].sort((a,b)=>(a.id < b.id ? -1:1))));

        this.setState({
            task:"",
            priority: "",
            todo,
        })
    };
    handleChange = (e) => {
        const { name, value } = e.target;
        const {error} = this.state;

        switch(name){
            case 'task':
                error[name]=value.length > 0 ?"":"task is required";
                break;
            case 'priority':
                error[name]=value.length > 0 ?"":" priority is required";
                break;
            default:
        }

        this.setState({
            [name]: value
        }, ()=>{
            console.log("NAME", name);
           
        });

        
    };
    onDeleteTask=async (id,e)=>{
        const todo = this.state.tasks;
        const orginalTodo = JSON.parse(localStorage.getItem("_todo"));
        const userId = JSON.parse(localStorage.getItem("_user")).id;

        // await Axios.delete("/tasks/"+id);

        const updatedTodo = todo.filter(t=>t.id !== id).sort((a,b)=>(a.id < b.id ? -1: 1 ));

        // const res = await Axios.get("/tasks");

        this.setState({
            tasks: updatedTodo
        });

        const notCurrentUserTodo = orginalTodo.filter(t=>t.userId!==userId);

        localStorage.setItem("_todo",JSON.stringify([...notCurrentUserTodo,...updatedTodo].sort((a,b)=>(a.id < b.id ? -1 :1 ))));
    };
    onCompleteTask=async(id,e)=>{

        const todo = this.state.tasks;
        const orginalTodo = JSON.parse(localStorage.getItem("_todo"));
        const userId = JSON.parse(localStorage.getItem("_user")).id;

        const updatedTodo = todo.map(t=> {
            if(t.id!==parseInt(id))return t
            return {...t, completed:true};
        });

        console.log("updated todo", updatedTodo);

        // await Axios.patch("/tasks/"+id, {
        //     completed:true
        // });
        // const res = await Axios.get("/tasks");

        this.setState({
            tasks: updatedTodo
        });
        const notCurrentUserTodo = orginalTodo.filter(t=>t.userId!==userId);

        localStorage.setItem("_todo",JSON.stringify([...notCurrentUserTodo,...updatedTodo].sort((a,b)=>a.id < b.id ? -1: 1)));
    };
    render() {
        return (
            <Box display="flex" flexDirection="row" justifyContent="start">
                <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} alignItems="stretch">
                    <Grid  item xs={4} md={3} >
                        <Sidebar {...this.props} />
                    </Grid>

                    <Grid item xs={8} md={9} >
                        <AddTask handleChange={this.handleChange} handleSubmit={this.handleSubmit}  taskDetails={{task:this.state.task, priority:this.state.priority, error:this.state.error}} />
                        <TaskItem sx={{ mt: 5 }} tasks={this.state.tasks} onDeleteTask={this.onDeleteTask} onCompleteTask={this.onCompleteTask} />
                    </Grid>
                </Grid>
            </Box>
        )
    }
}

export default withProtected(Dashboard)



