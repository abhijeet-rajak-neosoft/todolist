import React, { Component } from 'react';
import { Box, FormControl, InputLabel, OutlinedInput, TextField, InputAdornment, IconButton, CardContent, CardHeader, Card, Button, Typography, FormHelperText, Alert } from '@mui/material';

export class AddTask extends Component {


    // state = {
    //     task: "",
    //     priority: "",
    // }

    // handleChange = (e) => {
    //     const { name, value } = e.target;

    //     this.setState({
    //         [name]: value
    //     })
    // }
    render() {
        const { task, priority, error } = this.props.taskDetails;
        const id = JSON.parse(localStorage.getItem("_user")).id;
        // console.log("data", error);
        return (
            <>
                <Card sx={{ mx: "auto", mt: 5, mb:4, width: 500, }}>
                    <CardHeader align="center" sx={{}} title="Add Task">
                    </CardHeader>
                    <CardContent sx={{ maxWidth: 600 }}>
                        <Box sx={{ minWidth: 275, borderColor: 'secondary.main' }}>
                            <form onSubmit={(e)=>this.props.handleSubmit(id,e)}>

                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    <InputLabel htmlFor="outlined-adornment-task">Add Task</InputLabel>
                                    <OutlinedInput
                                        name="task"
                                        id="outlined-adornment-task"
                                        value={task}
                                        onChange={this.props.handleChange}
                                        label="Task Name"
                                    />
                                     {error.task.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{error.task}</FormHelperText>}

                                </FormControl>

                                <FormControl sx={{ mt: 1, width: '100%' }}>
                                    {/* <InputLabel htmlFor="outlined-adornment-task">Priority1</InputLabel> */}
                                    <TextField
                                        type="number"
                                        InputProps={{
                                            inputProps: {
                                                max: 5, min: 1
                                            }
                                        }}
                                        name="priority"
                                        id="outlined-adornment-task"
                                        value={priority}
                                        onChange={this.props.handleChange}
                                        label="Priority"
                                    />
                                    {error.priority.length > 0 && <FormHelperText sx={{ color: "red" }} id="component-error-text">{error.priority}</FormHelperText>}

                                </FormControl>


                                <Typography align='center'>
                                    <Button type="submit" sx={{ mt: 3, width: "50%" }} style={{ justifyContent: "center" }} variant="contained" color="success">
                                        Add Task
                                    </Button>
                                </Typography>


                            </form>
                        </Box>
                    </CardContent>
                </Card>
            </>
        )
    }
}

export default AddTask
